<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="/css/bootstrap.css" rel="stylesheet"/>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="container">
            <form enctype="multipart/form-data" role="form" action="{{route('createProduct')}}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Product name</label>
                    <input class="form-control" type="text" name="product_name"
                           value="{{Request::old('product_name')}}">
                    <p class="help-block">Enter product name.</p>
                </div>
                <div class="form-group">
                    <label>Quantity in stock</label>
                    <input class="form-control" type="text" name="quantity_in_stock"
                           value="{{Request::old('quantity_in_stock')}}">
                    <p class="help-block">Enter quantity in stock.</p>
                </div>
                <div class="form-group">
                    <label>Price per item</label>
                    <input class="form-control" type="text" name="price_per_item"
                           value="{{Request::old('price_per_item')}}">
                    <p class="help-block">Enter price per item.</p>
                </div>

                <button type="submit" class="btn btn-danger">Create</button>

            </form>
        </div>
        <div class="container">
            @if(isset($products) && count($products)>0)
                @include('data_table')
            @endif
        </div>
    </div>
</div>

<!-- BOOTSTRAP SCRIPTS -->
<script src="/js/bootstrap.js"></script>
</body>
</html>
