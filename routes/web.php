<?php


Route::get('/', [
    'uses' => 'ProductController@show',
    'as' => 'home'
]);

Route::post('/create_product', [
    'uses' => 'ProductController@create',
    'as' => 'createProduct'
]);
