<table id="table" class="table-responsive table text-center">
    <thead class="dataTables_head">
    <tr>
        <th>Id</th>
        <th>Product name</th>
        <th>Quantity in stock</th>
        <th>Price per item</th>
        <th>Datetime submitted</th>
        <th>Total value number</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->product_name}}</td>
            <td>{{$product->quantity_in_stock}}</td>
            <td>{{$product->price_per_item}}</td>
            <td>{{$product->created_at}}</td>
            <td>{{$product->quantity_in_stock * $product->price_per_item}}</td>


        </tr>
    @endforeach
    </tbody>
</table>