<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function show() {
        $products = Product::all();
        return view('test_form')->with('products', $products);
    }


    public function create(Request $request) {
        Product::create($request->toArray());
        $products = Product::all();
        return view('test_form')->with('products', $products);
    }
}
